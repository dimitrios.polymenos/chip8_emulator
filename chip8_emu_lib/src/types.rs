use std::fs::File;
use std::io::prelude::*;
use std::io::Error;

const SCREEN_WIDTH: u8 = 64;
const SCREEN_HEIGHT: u8 = 32;

pub struct EmuState {
	current_cycle: u64,
	stack: [u16; 24],
	idx_register: u16,
	program_counter: u16,
	keys: u16,
	memory: [u8; 4096],
	registers: [u8; 16],
	gfx: [[u8; 64]; 32],
	delay_timer: u8,
	sound_timer: u8,
	stack_pointer: u8,
}

const MAIN_OPCODE_TABLE: [fn(&mut EmuState, u16); 16] = [
	null_opcode,
	//jump_addr
	|state: &mut EmuState, opcode: u16| {
		state.program_counter = (opcode & 0x0FFF) - 2;
	},
	call_routine,
	//skip_next_if_eq
	|state: &mut EmuState, opcode: u16| {
		let vx = state.registers[((opcode & 0x0F00) >> 8) as usize];
		let kkk = (opcode & 0x00FF) as u8;
		state.program_counter += if vx == kkk { 2 } else { 0 };
	},
	//skip_next_if_not_eq
	|state: &mut EmuState, opcode: u16| {
		let vx = state.registers[((opcode & 0x0F00) >> 8) as usize];
		let kkk = (opcode & 0x00FF) as u8;
		state.program_counter += if vx != kkk { 2 } else { 0 };
	},
	//skip_next_Vx_eq_Vy
	|state: &mut EmuState, opcode: u16| {
		let vx = state.registers[((opcode & 0x0F00) >> 8) as usize];
		let vy = state.registers[((opcode & 0x00F0) >> 4) as usize];
		state.program_counter += if vx != vy { 2 } else { 0 };
	},
	modify_register,
	//add_Vx_kk
	|state: &mut EmuState, opcode: u16| {
		let kk = state.registers[(opcode & 0x00FF) as usize];
		state.registers[((opcode & 0x0F00) >> 8) as usize].wrapping_add(kk as u8);
	},
	//math_op
	|state: &mut EmuState, opcode: u16| {
		let exec = MATH_OPCODE_TABLE[(opcode & 0x000F) as usize];
		exec(
			state,
			((opcode & 0x0F00) >> 8) as usize,
			((opcode & 0x00F0) >> 4) as usize,
		);
	},
	null_opcode,
	set_idx_register,
	null_opcode,
	null_opcode,
	display_spr,
	null_opcode,
	null_opcode,
];

const MATH_OPCODE_TABLE: [fn(&mut EmuState, usize, usize); 16] = [
	//LD
	|state, x, y| {
		state.registers[x] = state.registers[y];
	},
	//OR
	|state, x, y| {
		state.registers[x] |= state.registers[y];
	},
	//AND
	|state, x, y| {
		state.registers[x] &= state.registers[y];
	},
	//XOR
	|state, x, y| {
		state.registers[x] ^= state.registers[y];
	},
	//ADD
	|state, x, y| {
		let vx = state.registers[x];
		let vy = state.registers[y];
		let addition_result = vx.overflowing_add(vy);
		state.registers[0xF] = if addition_result.1 { 1 } else { 0 };
		state.registers[x] = addition_result.0;
	},
	//SUB
	|state, x, y| {
		let vx = state.registers[x];
		let vy = state.registers[y];
		let (res, over) = vx.overflowing_sub(vy);
		state.registers[0xF] = if over { 1 } else { 0 };
		state.registers[x] = res;
	},
	//SHR
	|state, x, _y| {
		let vx = state.registers[x];
		let (res, over) = vx.overflowing_shr(1);
		state.registers[0xF] = if over { 1 } else { 0 };
		state.registers[x] = res;
	},
	//SUBN
	|state, x, y| {
		let vx = state.registers[x];
		let vy = state.registers[y];
		let (res, over) = vy.overflowing_sub(vx);
		state.registers[0xF] = if over { 1 } else { 0 };
		state.registers[x] = res;
	},
	//SHL
	|state, x, _y| {
		let vx = state.registers[x];
		let (res, over) = vx.overflowing_shl(1);
		state.registers[0xF] = if over { 1 } else { 0 };
		state.registers[x] = res;
	},
	unexpected_math_op,
	unexpected_math_op,
	unexpected_math_op,
	unexpected_math_op,
	unexpected_math_op,
	unexpected_math_op,
	unexpected_math_op,
];

fn null_opcode(_state: &mut EmuState, opcode: u16) {
	println!("unimplemented opcode received: {:x?}", opcode);
	unimplemented!();
}

fn unexpected_math_op(_state: &mut EmuState, _x: usize, _y: usize) {
	panic!("unexpected math op received");
}

fn call_routine(state: &mut EmuState, opcode: u16) {
	state.stack[state.stack_pointer as usize] = state.program_counter;
	state.stack_pointer += 1;
	//need to reduce pc by 2 since every op increments it by 2
	state.program_counter = (opcode & 0x0FFF) - 2;
}

fn modify_register(state: &mut EmuState, opcode: u16) {
	let register_num = ((opcode & 0x0F00) >> 8) as usize;
	let register_val = (opcode & 0x00FF) as u8;
	state.registers[register_num] = register_val;
}

fn set_idx_register(state: &mut EmuState, opcode: u16) {
	let register_val = opcode & 0x0FFF;
	state.idx_register = register_val;
}

fn display_spr(state: &mut EmuState, opcode: u16) {
	let x_coord_register = ((opcode & 0x0F00) >> 8) as usize;
	let y_coord_register = ((opcode & 0x00F0) >> 4) as usize;
	let x_coord = state.registers[x_coord_register] as u16;
	let y_coord = state.registers[y_coord_register] as u16;
	state.registers[0xF] = 0;
	let mut starting_mem_loc = state.idx_register as usize;
	for height in 0..(opcode & 0x000F) {
		for width in (0..8).rev() {
			let pixel_val = (state.memory[starting_mem_loc] >> width) & 1;
			let gfx_x = (x_coord + width) % SCREEN_WIDTH as u16;
			let gfx_y = (y_coord + height) % SCREEN_HEIGHT as u16;
			let gfx_pixel = &mut state.gfx[gfx_y as usize][gfx_x as usize];
			*gfx_pixel ^= pixel_val;
			if pixel_val == 1 && *gfx_pixel == pixel_val {
				state.registers[0xF] = 1;
			}
		}
		starting_mem_loc += 1;
	}
}

impl EmuState {
	fn reset_state(&mut self) {
		self.current_cycle = 0;
		self.stack = [0; 24];
		self.idx_register = 0;
		self.program_counter = 0x200;
		self.keys = 0;
		self.registers = [0; 16];
		self.gfx = [[0; 64]; 32];
		self.delay_timer = 0;
		self.sound_timer = 0;
		self.stack_pointer = 0;
	}

	fn fetch_opcode(&mut self) -> u16 {
		let ret = (self.memory[self.program_counter as usize] as u16) << 8
			| (self.memory[(self.program_counter + 1) as usize] as u16);
		self.program_counter += 2;
		//println!("fetched opcode {:x?}", ret);
		ret
	}
}

impl EmuState {
	pub fn new() -> EmuState {
		let ret = EmuState {
			current_cycle: 0,
			stack: [0; 24],
			idx_register: 0,
			program_counter: 0x200, //execution starts at 0x200
			keys: 0,
			memory: [0; 4096],
			registers: [0; 16],
			gfx: [[0; 64]; 32],
			delay_timer: 0,
			sound_timer: 0,
			stack_pointer: 0,
		};
		//initialise font
		ret
	}

	pub fn load_rom(&mut self, filename: &str) -> Result<(), Error> {
		let mut file = File::open(filename)?;
		file.read(&mut self.memory[0x200..])?;
		Ok(())
	}

	pub fn emulate_cycle(&mut self) {
		let opcode = self.fetch_opcode();
		let exec = MAIN_OPCODE_TABLE[((opcode & 0xF000) >> 12) as usize];
		exec(self, opcode);
		self.current_cycle += 1;
	}

	pub fn print_screen(&self) {
		for row in self.gfx.iter() {
			for pixel in row.iter() {
				print!("{}", pixel);
			}
			println!("");
		}
		println!("");
	}
}
