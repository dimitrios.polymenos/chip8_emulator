extern crate chip8_emu_lib as emu;

fn main() -> std::io::Result<()> {
	let mut st = emu::EmuState::new();
	st.load_rom("PONG")?;
	loop {
		st.emulate_cycle();
		//st.print_screen();
	}
	Ok(())
}
